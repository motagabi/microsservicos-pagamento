package br.com.cartao.pagamento.controllers;

import br.com.cartao.pagamento.DTOs.PagamentoDTO;
import br.com.cartao.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController//transforma em classe controladora do spring
@RequestMapping("/pagamento")
public class PagamentoController {
    @Autowired
    PagamentoService pagamentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoDTO cadastrarPagamento(@RequestBody @Valid PagamentoDTO pagamentoDTO) {
        return pagamentoService.salvarPagamento(pagamentoDTO);
    }

    @GetMapping("/{idCartao}")
    @ResponseStatus(HttpStatus.OK)
    public List<PagamentoDTO> listarPagamentosPorIdCartao(@RequestBody @PathVariable(name = "idCartao") int idCartao) {
        return pagamentoService.listarPagamentosPorIdCartao(idCartao);

    }
}
