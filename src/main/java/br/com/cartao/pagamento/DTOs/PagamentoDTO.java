package br.com.cartao.pagamento.DTOs;

import com.sun.istack.NotNull;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class PagamentoDTO {

    private int id;

    private int cartaoId;

    @NotBlank(message = "Descricao precisa ser preenchida.")
    @NotNull()
    @Size(min = 5, message = "Descricao precisa ser preenchida com no mínio 5 letras.")
    private String descricao;

    @DecimalMin(value = "1.0", message = "Valor precisa ser maior que R$1.00.")
    @Digits(integer = 6, fraction = 2, message = "Valor fora do padrão.")
    private Double valor;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCartaoId() {
        return cartaoId;
    }

    public void setCartaoId(int cartaoId) {
        this.cartaoId = cartaoId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
