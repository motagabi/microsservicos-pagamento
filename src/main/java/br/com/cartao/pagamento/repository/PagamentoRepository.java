package br.com.cartao.pagamento.repository;

import br.com.cartao.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findAllByCartaoId(int idCartao);
}
