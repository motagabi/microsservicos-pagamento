package br.com.cartao.pagamento.services;

import br.com.cartao.pagamento.DTOs.PagamentoDTO;
import br.com.cartao.pagamento.clients.Cartao;
import br.com.cartao.pagamento.clients.CartaoClient;
import br.com.cartao.pagamento.models.Pagamento;
import br.com.cartao.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentoService {
    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoClient cartaoClient;


    public PagamentoDTO salvarPagamento(PagamentoDTO pagamentoDTO) {
        Cartao cartao = cartaoClient.getById(pagamentoDTO.getCartaoId());

        if (cartao != null) {
            if(cartao.isAtivo()) {
                Pagamento pagamento = new Pagamento();
                pagamento.setDescricao(pagamentoDTO.getDescricao());
                pagamento.setValor(pagamentoDTO.getValor());
                pagamento.setCartaoId(pagamentoDTO.getCartaoId());
                pagamentoRepository.save(pagamento);
                return this.transformarPagamentoEmPagamentoDTO(pagamento);
            }else{
                throw new RuntimeException("Cartão "+ pagamentoDTO.getCartaoId() +" não ativado! Ative pelo no App");
            }
        } else {
            throw new RuntimeException("Cartão "+ pagamentoDTO.getCartaoId() +" não encontrado para efetuar o Pagamento.");
        }
    }

    public List<PagamentoDTO> listarPagamentosPorIdCartao(int idCartao) {
        Cartao cartao = cartaoClient.getById(idCartao);
        if (cartao != null) {
            return this.transformarListaDePagamentosEmPagamentoDTO((List<Pagamento>) pagamentoRepository.findAllByCartaoId(idCartao));
        } else {
            throw new RuntimeException("Cartão não encontrado.");
        }
    }

    private PagamentoDTO transformarPagamentoEmPagamentoDTO(Pagamento pagamento) {
        PagamentoDTO pagamentoDTO = new PagamentoDTO();

        pagamentoDTO.setId(pagamento.getId());
        pagamentoDTO.setCartaoId(pagamento.getCartaoId());
        pagamentoDTO.setDescricao(pagamento.getDescricao());
        pagamentoDTO.setValor(pagamento.getValor());

        return pagamentoDTO;
    }

    private List<PagamentoDTO> transformarListaDePagamentosEmPagamentoDTO(List<Pagamento> pagamentos) {
        List<PagamentoDTO> listaPagamentoDTO = new ArrayList<>();
        PagamentoDTO pagamentoDTO;

        for (Pagamento pagamento : pagamentos) {
            pagamentoDTO = new PagamentoDTO();

            pagamentoDTO.setId(pagamento.getId());
            pagamentoDTO.setCartaoId(pagamento.getCartaoId());
            pagamentoDTO.setDescricao(pagamento.getDescricao());
            pagamentoDTO.setValor(pagamento.getValor());

            listaPagamentoDTO.add(pagamentoDTO);
        }

        return listaPagamentoDTO;
    }
}
