package br.com.cartao.pagamento.clients;

import feign.codec.ErrorDecoder;
import org.springframework.context.annotation.Bean;

public class PagamentoClientConfiguration {
    @Bean
    public ErrorDecoder getCarClientDecoder() {
        return new CartaoClientDecoder();
    }
}
