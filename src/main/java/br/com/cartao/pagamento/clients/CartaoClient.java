package br.com.cartao.pagamento.clients;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CARTAO")
public interface CartaoClient {
    @GetMapping("cartao/{id}")
    Cartao getById(@PathVariable int id);
}

